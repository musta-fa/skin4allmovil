package es.inftel.bd;

import es.inftel.modelo.AccionLlamar;
import es.inftel.modelo.Componente;
import es.inftel.modelo.EnviarMensaje;
import es.inftel.singleton.UsuarioSingleton;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ComponenteSQLite extends SQLiteOpenHelper{
	
	private static final String DATABASE_CREATE = "CREATE TABLE Componente (" +
			"id INTEGER primary key, " +			
			"nombreAccion TEXT, " +
			"idApp INTEGER, " +
			"nombreEvento TEXT, " +
			"posicion INTEGER, " +
			"tipoEvento INTEGER, " +
			"persona TEXT ," +
			"telefono TEXT ," +
			"texto TEXT, " +
			"FOREIGN KEY (idApp) REFERENCES App (id));";
	
	
	private static final String DATABASE_DROP = "DROP TABLE IF EXISTS Componente";	
	

	public ComponenteSQLite(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}	

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion < newVersion){
			db.execSQL(DATABASE_CREATE);
			db.execSQL(DATABASE_DROP);			
		}		
	}
	
	public void insertarComponente(int id, String nombreAccion, int idApp, String nombreEvento, int posicion, int tipoEvento, String persona, String telefono, String texto){
		String DATABASE_INSERT = "INSERT INTO Componente (id, nombreAccion, idApp, nombreEvento, posicion, tipoEvento, persona, telefono, texto) " +
				"VALUES (" +
				id +
				", '" + nombreAccion + "'" +
				", '" + idApp + "'" +
				", '" + nombreEvento + "'" +
				", '" + posicion + "'" +
				", '" + persona + "'" +
				", '" + telefono + "'" +
				", '" + texto + "'" +
				", '" + tipoEvento +"')";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.execSQL(DATABASE_INSERT);
		db.close();
		
		Log.i("ComponenteSQLite", "Componente insertado!");
	}
	
	public void insertarComponente(Componente c){
		
		if(c instanceof EnviarMensaje){
			EnviarMensaje em = (EnviarMensaje) c;
			String DATABASE_INSERT = "INSERT INTO Componente (id, nombreAccion, idApp, nombreEvento, posicion, tipoEvento, persona, telefono, texto) " +
					"VALUES (" +
					em.getId() +
					", '" + em.getNombreAccion() + "'" +
					", '" + em.getIdApp() + "'" +
					", '" + em.getNombreEvento() + "'" +
					", '" + em.getPosicion() + "'" +
					", '" + em.getTipoEvento() + "'" +
					", '" + em.getPersona() + "'" +
					", '" + em.getTelefono() + "'" +
					", '" + em.getTexto() + "')";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			db.execSQL(DATABASE_INSERT);
			db.close();
			
		}
		else if(c instanceof AccionLlamar){
			AccionLlamar acllam = (AccionLlamar) c;
			String DATABASE_INSERT = "INSERT INTO Componente (id, nombreAccion, idApp, nombreEvento, posicion, tipoEvento, persona, telefono) " +
					"VALUES (" +
					acllam.getId() +
					", '" + acllam.getNombreAccion() + "'" +
					", '" + acllam.getIdApp() + "'" +
					", '" + acllam.getNombreEvento() + "'" +
					", '" + acllam.getPosicion() + "'" +
					", '" + acllam.getTipoEvento() + "'" +
					", '" + acllam.getPersona() + "'" +
					", '" + acllam.getTelefono() + "')";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			db.execSQL(DATABASE_INSERT);	
			db.close();
			
		}
		else{			
			String DATABASE_INSERT = "INSERT INTO Componente (id, nombreAccion, idApp, nombreEvento, posicion, tipoEvento) " +
					"VALUES (" +
					c.getId() +
					", '" + c.getNombreAccion() + "'" +
					", '" + c.getIdApp() + "'" +
					", '" + c.getNombreEvento() + "'" +
					", '" + c.getPosicion() + "'" +
					", '" + c.getTipoEvento() + "')";
			
			SQLiteDatabase db = this.getWritableDatabase();
			
			db.execSQL(DATABASE_INSERT);
			db.close();
		}		
	}	
	
	public void obtenerComponentes(){
		String DATABASE_SELECTALL = "SELECT * FROM Componente";
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(DATABASE_SELECTALL, null);
		
		if(cursor.moveToFirst()){	
			
			UsuarioSingleton us = UsuarioSingleton.getInstance();
			
			do {
				if(cursor.getString(8) != null){ //Si texto no está vacío es de tipo ENVIARMENSAJE
					EnviarMensaje em = new EnviarMensaje();
					em.setId(cursor.getInt(0));
					em.setNombreAccion(cursor.getString(1));
					em.setIdApp(cursor.getInt(2));
					em.setNombreEvento(cursor.getString(3));
					em.setPosicion(cursor.getInt(4));
					em.setTipoEvento(cursor.getInt(5));
					em.setPersona(cursor.getString(6));
					em.setTelefono(cursor.getString(7));
					em.setTexto(cursor.getString(8));
					
					us.getUsuario().addComponenteApp(em);
				}
				else if(cursor.getString(7) != null){ //Si texto vacio y telf no vacio = LLAMAR_A
					AccionLlamar allam = new AccionLlamar();
					allam.setId(cursor.getInt(0));
					allam.setNombreAccion(cursor.getString(1));
					allam.setIdApp(cursor.getInt(2));
					allam.setNombreEvento(cursor.getString(3));
					allam.setPosicion(cursor.getInt(4));
					allam.setTipoEvento(cursor.getInt(5));
					allam.setPersona(cursor.getString(6));
					allam.setTelefono(cursor.getString(7));	
					
					us.getUsuario().addComponenteApp(allam);
					
				}
				else { //Es un componente normal
					Componente c = new Componente();
					c.setId(cursor.getInt(0));
					c.setNombreAccion(cursor.getString(1));
					c.setIdApp(cursor.getInt(2));
					c.setNombreEvento(cursor.getString(3));
					c.setPosicion(cursor.getInt(4));
					c.setTipoEvento(cursor.getInt(5));
					
					us.getUsuario().addComponenteApp(c);
				}							
				
				Log.i("ComponenteSQLite", "Componente...");
			} while(cursor.moveToNext());
		}
		
		db.close();
	}	
}
