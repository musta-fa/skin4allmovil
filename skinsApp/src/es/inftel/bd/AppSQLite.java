package es.inftel.bd;

import es.inftel.modelo.App;
import es.inftel.singleton.UsuarioSingleton;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AppSQLite extends SQLiteOpenHelper{
	
	private static final String DATABASE_CREATE = "CREATE TABLE App (" +
			"id INTEGER primary key, " +			
			"nombre TEXT, " +
			"icono TEXT);";
	
	private static final String DATABASE_DROP = "DROP TABLE IF EXISTS App";
	
	
	public AppSQLite(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
			super(context, name, factory, version, errorHandler);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
;		db.execSQL(DATABASE_CREATE);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int versionAnterior, int versionNueva) { 		
		db.execSQL(DATABASE_DROP);
		db.execSQL(DATABASE_CREATE);
	}
	
	public void insertarApp(int id, String nombre, String icono){
		String DATABASE_INSERT = "INSERT INTO App (id, nombre, icono) " +
				"VALUES (" +
				id +
				", '" + nombre + "'" +
				", '" + icono +"')";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.execSQL(DATABASE_INSERT);
		db.close();
	}
	
	public void insertarApp(App app){
		String DATABASE_INSERT = "INSERT INTO App (id, nombre, icono) " +
				"VALUES (" +
				app.getIdApp() +
				", '" + app.getNombre() + "'" +
				", '" + app.getIcono() +"')";
		
		SQLiteDatabase db = this.getWritableDatabase();
		
		db.execSQL(DATABASE_INSERT);
		db.close();
		
		Log.i("AppSQLite", "App insertada!");
	}	
	
	public void obtenerApps(){
		String DATABASE_SELECTALL = "SELECT * FROM App";
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(DATABASE_SELECTALL, null);
		
		if(cursor.moveToFirst()){	
			
			UsuarioSingleton us = UsuarioSingleton.getInstance();			
			
			do {				
				
				App app = new App();
				app.setIdApp(cursor.getInt(0));
				app.setNombre(cursor.getString(1));
				app.setIcono(cursor.getString(2));
				
				us.getUsuario().addApp(app);				
				
			} while(cursor.moveToNext());
		}
		
		db.close();
	}
	

}
