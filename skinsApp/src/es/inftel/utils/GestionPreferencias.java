package es.inftel.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class GestionPreferencias {

	private SharedPreferences preferences;
    private Editor editor;    
    
    // SharedPreferences nombre de archivo
    private static final String PREF_NAME = "SKINS4ALL_PREF";
          
    // Nombre de usuario
    public static final String KEY_NAME = "name";
    
    // Email de usuario
    public static final String KEY_PASSWORD = "pass";       
    
    private static final String IS_LOGGED = "is_logged_in";
    
    
    public GestionPreferencias(Context contexto){
        preferences = contexto.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = preferences.edit();               	
    }
    
    /**
     * Loguear a un usuario guardando sus datos en SharedPreferences
     * */
    public void loginUsuario(String nombre, String email){ 
        editor.putString(KEY_PASSWORD, email);        
        editor.putString(KEY_NAME, nombre);    
        editor.putBoolean(IS_LOGGED, true);
        editor.commit();
    }    
    
    /**
     * Logout. Borra toda la información de SharedPreferences
     * */
    public void logoutUsuario(){        
        editor.clear();
        editor.commit();
    }    
    
    public void anadirNombre(String nombre){
    	editor.putString(KEY_NAME, nombre);
    	editor.commit();    	    	
    }	
    
    public void anadirPassword(String pass){
    	editor.putString(KEY_PASSWORD, pass);
    	editor.commit();    	    	
    }     
    
    public String getNombre(){
    	return preferences.getString(KEY_NAME, "");
    }
    
    public String getPassword(){
    	return preferences.getString(KEY_PASSWORD, "");
    }
    
    public boolean checkLoginUsuario(){
    	return preferences.getBoolean(IS_LOGGED, false);
    } 
}

