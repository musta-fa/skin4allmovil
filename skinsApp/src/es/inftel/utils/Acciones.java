package es.inftel.utils;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import es.inftel.modelo.AccionLlamar;
import es.inftel.modelo.EnviarMensaje;
import es.inftel.skinsapp.R;

public class Acciones {
		
	public void abrirFacebook(Activity activity){		
		Intent postOnWall = new Intent(Intent.ACTION_SEND);
		postOnWall.setType("text/plain");
		postOnWall.putExtra(android.content.Intent.EXTRA_TEXT, R.string.escribirRedSocial);
		
		//Recuperamos las aplicaciones para las que el usuario tiene permisos
		PackageManager pm = activity.getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(postOnWall, PackageManager.MATCH_DEFAULT_ONLY);
		
		boolean found = false;
		for (final ResolveInfo app : activityList) {
			if ((app.activityInfo.name).contains("facebook")) {				
				postOnWall.setClassName(app.activityInfo.packageName, app.activityInfo.name);				
				found = true;
				break;
			}
		}
		
		if(found){
			postOnWall.addCategory(Intent.CATEGORY_LAUNCHER);
			postOnWall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);			
			activity.startActivity(postOnWall);			
		}
		else{						 
			new AlertDialog.Builder(activity)
				.setTitle(R.string.error)
			    .setMessage(R.string.errorapp)
			    .setNeutralButton(R.string.cerrar, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	
			        }
			     })
			    .setIcon(R.drawable.ic_stat_alert)
			    .show();
		}		
	}
	
	public void abrirTwitter(Activity activity){
		
		Intent sendTweet = new Intent(Intent.ACTION_SEND);
		sendTweet.setType("text/plain");
		sendTweet.putExtra(android.content.Intent.EXTRA_TEXT, R.string.escribirRedSocial);
		
		//Recuperamos las aplicaciones para las que el usuario tiene permisos
		PackageManager pm = activity.getPackageManager();
		List<ResolveInfo> activityList = pm.queryIntentActivities(sendTweet, PackageManager.MATCH_DEFAULT_ONLY);
		
		boolean found = false;
		for (final ResolveInfo app : activityList) {
			if ((app.activityInfo.name).contains("twitter")) {				
				sendTweet.setClassName(app.activityInfo.packageName, app.activityInfo.name);				
				found = true;
				break;
			}
		}
		
		if(found){
			sendTweet.addCategory(Intent.CATEGORY_LAUNCHER);
			sendTweet.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);			
			activity.startActivity(sendTweet);			
		}
		else{
			new AlertDialog.Builder(activity)
			.setTitle(R.string.error)
		    .setMessage(R.string.errorapp)
		    .setNeutralButton(R.string.cerrar, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	
		        }
		     })
		    .setIcon(R.drawable.ic_stat_alert)
		    .show(); 
		}	
	}	
	
	
	public void realizarLlamada(Activity activity, AccionLlamar allam){		
		
        try {
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        String tel = "tel:"+ allam.getTelefono();
	        callIntent.setData(Uri.parse(tel));
	        activity.startActivity(callIntent);
	        
        } catch (ActivityNotFoundException activityException) {
        	Log.e("Acciones", "Call failed", activityException);
        }				
	}
	
	public void enviarSMS(Activity activity, EnviarMensaje sms) {		
        try {
            String tel = sms.getTelefono();
			Intent smsSIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:" + tel));
			smsSIntent.putExtra("sms_body", sms.getTexto());		
			activity.startActivity(smsSIntent);
            
        } catch (ActivityNotFoundException activityException) {
            Log.e("Acciones", "Fallo al enviar sms.", activityException);
        }		
	}
	

}
