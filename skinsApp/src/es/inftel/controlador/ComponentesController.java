package es.inftel.controlador;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import es.inftel.modelo.AccionLlamar;
import es.inftel.modelo.App;
import es.inftel.modelo.Componente;
import es.inftel.modelo.EnviarMensaje;
import es.inftel.singleton.UsuarioSingleton;

public class ComponentesController extends PeticionesAbstract {
	
	public ComponentesController(){}
	
	public String obtenerComponentes(String idUsuario){
		
		String respuesta = this.doGet(es.inftel.utils.Constants.URL_COM + "idusuario=" + idUsuario);
		
		if(respuesta.equalsIgnoreCase("ERROR")){
			return "ERROR";
		}
		else{
			JSONArray arrayJson;
			try {
				arrayJson = new JSONArray(respuesta);
				
				parsearJSON(arrayJson);
				
			} catch (JSONException e) {
				Log.e("ComponentesController", "Error al parsear JSON.");
				e.printStackTrace();
			}	
			
			return "OK";
		}		
	}
	
	
	public void parsearJSON(JSONArray array)
	{
		UsuarioSingleton us = UsuarioSingleton.getInstance();
		for(int i = 0; i < array.length(); i++)
		{			
			App app = new App();		
			
			try 
			{
				JSONObject componenteJSON = (JSONObject) array.get(i);
				
				JSONObject appJSON = componenteJSON.getJSONObject("idapp");	
				int idApp = appJSON.getInt("id");
				app.setIdApp(idApp);
				app.setIcono(appJSON.getString("icono"));
				app.setNombre(appJSON.getString("nombre"));
				
				us.getUsuario().addApp(app);
								
				
				int idComponente = componenteJSON.getInt("id");
				
				String nombreAccion = componenteJSON.getString("nombreAccion");
				int tipoEvento = componenteJSON.getInt("tipoevento");
				int posicion = componenteJSON.getInt("posicion");
				String nombreEvento = null;
				if(componenteJSON.has("nombreevento"))
				{
					nombreEvento = componenteJSON.getString("nombreevento");
				}

				if (componenteJSON.has("accionLlamar"))
				{
					AccionLlamar ac = new AccionLlamar();

					ac.setId(idComponente);
					ac.setIdApp(idApp);
					ac.setNombreAccion(nombreAccion);
					ac.setNombreEvento(nombreEvento);
					ac.setPosicion(posicion);
					ac.setTipoEvento(tipoEvento);
					
					JSONObject accionLlamarJSON = componenteJSON.getJSONObject("accionLlamar");
					ac.setPersona(accionLlamarJSON.getString("persona"));
					ac.setTelefono(accionLlamarJSON.getString("telefono"));
					
					
					us.getUsuario().addComponenteApp(ac);										
				}
				else
				{
					if(componenteJSON.has("accionEnviarmensaje"))
					{
						
						EnviarMensaje em = new EnviarMensaje();
						em.setId(idComponente);
						em.setIdApp(idApp);
						em.setNombreAccion(nombreAccion);
						em.setNombreEvento(nombreEvento);
						em.setPosicion(posicion);
						em.setTipoEvento(tipoEvento);										
						
						JSONObject enviarMensajeJSON = componenteJSON.getJSONObject("accionEnviarmensaje");
						em.setPersona(enviarMensajeJSON.getString("nombre"));
						em.setTelefono(enviarMensajeJSON.getString("telefono"));
						em.setTexto(enviarMensajeJSON.getString("texto"));
						
						us.getUsuario().addComponenteApp(em);						
					}
					else
					{
						Componente c = new Componente();
						c.setId(idComponente);
						c.setIdApp(idApp);
						c.setNombreAccion(nombreAccion);
						c.setNombreEvento(nombreEvento);
						c.setPosicion(posicion);
						c.setTipoEvento(tipoEvento);
						
						us.getUsuario().addComponenteApp(c);												
					}
					
				}
												
			} catch (JSONException e) {
				Log.e("ComponentesController", "JSON error");
				e.printStackTrace();
			}

		}
	}

}
