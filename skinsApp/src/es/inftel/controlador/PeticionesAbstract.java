package es.inftel.controlador;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public abstract class PeticionesAbstract {
	
	public String doGet(String url) {		
		String resultado = "";		
		
		HttpParams httpParameters = new BasicHttpParams();
		
		// Timeout hasta que la conexion se establece
		int timeoutConnection = 10000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		
		// Socket timeout : tiempo de espera hasta que reciba datos 
		int timeoutSocket = 10000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);		
		
		HttpClient httpclient = new DefaultHttpClient(httpParameters);
		HttpGet httpget = new HttpGet(url);								
		HttpResponse response;
		
		try {
			
			response = httpclient.execute(httpget);			
		  	HttpEntity entity = response.getEntity();
		  	if(entity != null) {
			  	resultado = EntityUtils.toString(entity);			  
			  	return resultado;		  		
		  	}
		  	return "ERROR";
		  	
		} catch (ClientProtocolException e) {
			Log.e("PeticionesAbstract", "HTTP error.");
			e.printStackTrace();
		} catch (IOException e) {
			Log.e("PeticionesAbstract", "Error de conexion.");
			e.printStackTrace();
		}						
		
		return resultado;		
	}	

}
