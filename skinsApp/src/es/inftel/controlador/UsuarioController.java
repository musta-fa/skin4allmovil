package es.inftel.controlador;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import es.inftel.modelo.Usuario;
import es.inftel.singleton.UsuarioSingleton;

public class UsuarioController extends PeticionesAbstract {
	
	public UsuarioController(){}
	
	public String obtenerUsuario(String nombre, String pass){
		
		String respuesta = this.doGet(es.inftel.utils.Constants.URL_LOG + "nombre=" + nombre + "&pass=" + pass);
		
		if(respuesta.equalsIgnoreCase("ERROR")) {			
			return "ERROR";
		}
		else {
			JSONObject usuarioJson;
			try {
				usuarioJson = new JSONObject(respuesta);
				
				parsearJSON(usuarioJson);
				
			} catch (JSONException e) {
				Log.e("UsuarioController", "Error al parsear JSON.");
				e.printStackTrace();
			}
			return "OK";
		}			
	}
	
	private void parsearJSON (JSONObject usuarioJSON)
	{
		Usuario u = new Usuario();
		
			try 
			{
				u.setId(usuarioJSON.getInt("id"));
				u.setNombre(usuarioJSON.getString("nombre"));
				u.setPassword(usuarioJSON.getString("password"));
				
				UsuarioSingleton us = UsuarioSingleton.getInstance();

				us.setUsuario(u);
																
			} catch (JSONException e) {
				Log.e("UsuarioController", "JSON error.");
				e.printStackTrace();
			}
	}
}
