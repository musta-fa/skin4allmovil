package es.inftel.modelo;

import java.util.ArrayList;
import java.util.HashMap;

public class Usuario {
	private int id;
	private String nombre;
	private String password;
	private HashMap<Integer, App> mapAPP;

	public Usuario()
	{
		mapAPP = new HashMap<Integer, App>();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public HashMap<Integer, App> getListaAPP() {
		return mapAPP;
	}
	
	public ArrayList<App> getListaApp(){
		return new ArrayList<App>(mapAPP.values());
	}
	
	public void setListaAPP(HashMap<Integer, App> listaAPP) {
		this.mapAPP = listaAPP;
	}
	
	public void addApp(App app)
	{
		if(!mapAPP.containsKey(app.getIdApp())) 
		{
			mapAPP.put(app.getIdApp(), app);			
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addComponenteApp(Componente c)
	{
		boolean componenteAnadido = false;
		
		for(Componente comp : mapAPP.get(c.getIdApp()).getListaComponentes()){
			if(comp.getId() == c.getId()){
				componenteAnadido = true;
				break;
			} 			
		}
		if(componenteAnadido == false) mapAPP.get(c.getIdApp()).addComponente(c);		
	}
	
	public Componente getComponenteApp(int idApp){
		return mapAPP.get(idApp).getListaComponentes().get(0);
	}
	
	public void destruirAppsModelo(){
		mapAPP.clear();
	}
	
	public App getAppPorId(int id){
		return mapAPP.get(id);
	}

}
