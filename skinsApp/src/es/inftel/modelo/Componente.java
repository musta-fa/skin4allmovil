package es.inftel.modelo;

public class Componente {
	
	private int id;
	private String nombreAccion;
	private int idApp;
	private  String nombreEvento;
	private int posicion;
	private int tipoEvento;
	
	
	public String getNombreAccion() {
		return nombreAccion;
	}
	
	public void setNombreAccion(String nombreAccion) {
		this.nombreAccion = nombreAccion;
	}
	
	public int getIdApp() {
		return idApp;
	}
	
	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}
	
	public String getNombreEvento() {
		return nombreEvento;
	}
	
	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
	}
	
	public int getPosicion() {
		return posicion;
	}
	
	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}
	
	public int getTipoEvento() {
		return tipoEvento;
	}
	
	public void setTipoEvento(int tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
}
