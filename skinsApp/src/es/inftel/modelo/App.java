package es.inftel.modelo;

import java.util.ArrayList;

public class App {
	int idApp;
	String nombre;
	String icono;
	ArrayList<Componente> listaComponentes;
	
	
	public App()
	{
		listaComponentes = new ArrayList<Componente>();
	}
	
	public App(int idApp, String nombre, String icono){
		this.idApp = idApp;
		this.nombre = nombre;
		this.icono = icono;
	}	
	
	public int getIdApp() {
		return idApp;
	}
	public void setIdApp(int idApp) {
		this.idApp = idApp;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIcono() {
		return icono;
	}
	public void setIcono(String icono) {
		this.icono = icono;
	}
	public ArrayList<Componente> getListaComponentes() {
		return listaComponentes;
	}
	public void setListaComponentes(ArrayList<Componente> listaComponentes) {
		this.listaComponentes = listaComponentes;
	}
	
	public void addComponente(Componente c)
	{
		listaComponentes.add(c);
	}
	
	public Componente getComponentePorGesto(String tipoGesto)
	{
		int i = 0;
		while(i < listaComponentes.size())
		{
			Componente c = listaComponentes.get(i);
			String nombreEvento = c.getNombreEvento();
			
			if((nombreEvento != null) && (nombreEvento.equalsIgnoreCase(tipoGesto)))
			{
				return c;
			}
			i++;
		}
		return null;
	}
	
}
