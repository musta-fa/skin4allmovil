package es.inftel.asynctask;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import es.inftel.controlador.UsuarioController;
import es.inftel.singleton.UsuarioSingleton;
import es.inftel.skinsapp.R;
import es.inftel.utils.GestionPreferencias;



public class ObtenerUsuarioAsyncTask extends AsyncTask<String, Void, String> {
	
	private Context contexto;
	private Activity activity;	
	
	
	public Context getContexto() {
		return contexto;
	}

	public void setContexto(Context contexto) {
		this.contexto = contexto;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@Override 
	protected String doInBackground(String... params) {

			 UsuarioController uc = new UsuarioController();			 
			 return uc.obtenerUsuario(params[0], params[1]);
	}

	@Override
	protected void onPostExecute(String result) {		
		super.onPostExecute(result);
		
		if(result.equalsIgnoreCase("ERROR")){
			new AlertDialog.Builder(activity)
			.setTitle(R.string.error)
		    .setMessage(R.string.errorlogin)
		    .setNeutralButton(R.string.cerrar, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 		        
		        }
		     })
		    .setIcon(R.drawable.ic_stat_alert)
		    .show(); 			
		}
		else {
	        UsuarioSingleton us = UsuarioSingleton.getInstance();	        	        	        
	        if(us.getUsuario() != null) {
	            GestionPreferencias pref = new GestionPreferencias(contexto.getApplicationContext());
	            pref.loginUsuario(us.getUsuario().getNombre(), us.getUsuario().getPassword());  
	            			        
		        ObtenerComponentesAsyncTask tarea2 = new ObtenerComponentesAsyncTask();		        
		        tarea2.setContexto(contexto);
		        int idprueba = us.getUsuario().getId();
		        String idString = String.valueOf(idprueba);
		        
		        tarea2.execute(idString);	  		  
	    		
			}			
		}		
	}
}
