package es.inftel.asynctask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import es.inftel.controlador.ComponentesController;
import es.inftel.skinsapp.ApplicationListActivity;

public class ObtenerComponentesAsyncTask extends AsyncTask<String, Void, String> {
	private Context contexto;	
	
	public Context getContexto() {
		return contexto;
	}

	public void setContexto(Context contexto) {
		this.contexto = contexto;
	}		
    
	@Override 
	protected String doInBackground(String... params) {
		ComponentesController cc = new ComponentesController();
		cc.obtenerComponentes(params[0]);
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {		
        super.onPostExecute(result);
        
    	Intent i = new Intent(contexto, ApplicationListActivity.class);
    	i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);    		
    	contexto.startActivity(i);        	        
	}	
}
