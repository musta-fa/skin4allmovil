package es.inftel.skinsapp;

import es.inftel.singleton.UsuarioSingleton;
import es.inftel.utils.GestionPreferencias;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ApplicationListActivity extends FragmentActivity implements
		ApplicationListFragment.Callbacks {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_application_list);
	}

	@Override
	public void onItemSelected(long id) {
			Intent detailIntent = new Intent(this, ApplicationDetailActivity.class);
			detailIntent.putExtra(BotonesDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list_activity, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {			
		switch(item.getItemId()){				
			case R.id.btLogout:
				GestionPreferencias pref = new GestionPreferencias(this.getApplicationContext());
				pref.logoutUsuario();
				UsuarioSingleton us = UsuarioSingleton.getInstance();
				us.logout();								
				
				Intent intent2 = new Intent(this, MainActivity.class);
				intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |  Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); 
				startActivity(intent2);
				this.finish();
								
				break;				
			}	

		return true;
	}
}
