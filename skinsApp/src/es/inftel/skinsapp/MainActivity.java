package es.inftel.skinsapp;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import es.inftel.asynctask.ObtenerUsuarioAsyncTask;
import es.inftel.bd.AppSQLite;
import es.inftel.bd.ComponenteSQLite;
import es.inftel.singleton.UsuarioSingleton;
import es.inftel.utils.GestionPreferencias;
import es.inftel.utils.TestConexionDispositivo;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
                     
        GestionPreferencias pref = new GestionPreferencias(getApplicationContext());   
        
        TestConexionDispositivo conexion = new TestConexionDispositivo();        

        if(pref.checkLoginUsuario()){ //Si esta logueado        	
            if(conexion.hayConexion(getApplicationContext())){ //Construimos modelo desde REST  
            	Log.i("MainActivity", "Conexión y Logueo");

            	ObtenerUsuarioAsyncTask tarea = new ObtenerUsuarioAsyncTask();
            	tarea.setActivity(this);
		        tarea.setContexto(getApplicationContext());		        		        
		        tarea.execute(pref.getNombre(), pref.getPassword());
		        
		        finish();

            }
            else{ //Construimos modelo desde la BD       		
            	Log.i("MainActivity", "NO Conexión y Logueo");
            	
            	UsuarioSingleton us = UsuarioSingleton.getInstance();
            	us.getUsuario().destruirAppsModelo();
            	
		        AppSQLite appdb = new AppSQLite(this, "dbapp", null, 1, null); 		        
		        appdb.obtenerApps();
		        
		        ComponenteSQLite componentedb = new ComponenteSQLite(this, "dbcomponente", null, 1, null);
		        componentedb.obtenerComponentes();		        
		        
		        
		        Intent i = new Intent(this, ApplicationListActivity.class);
		        startActivity(i);
		        
		        finish();
            }        	        	
        }
        else{ //Si NO esta logueado
            if(conexion.hayConexion(getApplicationContext())){  //Si hay conexion vamos a Login        
            	Log.i("MainActivity", "Conexión y NO Logueo");
            	
            	Intent intent = new Intent(this, LoginActivity.class);
            	startActivity(intent);
            	
            	finish();
            }
            else{ 
            	
            	//No puede ejecutar la aplicación.
    			new AlertDialog.Builder(this)
    			.setTitle(R.string.error)
    		    .setMessage(R.string.errornoexec)
    		    .setNeutralButton(R.string.cerrar, new DialogInterface.OnClickListener() {
    		        public void onClick(DialogInterface dialog, int which) { 
    		        	System.exit(0);
    		        }
    		     })
    		    .setIcon(R.drawable.ic_stat_alert)
    		    .show();    			
            }        	            
        }                
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {        
        getMenuInflater().inflate(R.menu.main, menu);
        return true;                
    }
    

    
}
