package es.inftel.skinsapp;

import es.inftel.asynctask.ObtenerUsuarioAsyncTask;
import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {

	private String usuario;
	private String pass;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		ActionBar ab = getActionBar();
		ab.hide();
        
        setContentView(R.layout.activity_login);
        
        
        Button btLogin = (Button) findViewById(R.id.btLogin);
        btLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
		        EditText etusuario = (EditText) findViewById(R.id.etUsuario);
		        usuario = etusuario.getText().toString();
		        EditText etpass = (EditText) findViewById(R.id.etPassword);
		        pass = etpass.getText().toString();			       		       
		        
		        if(usuario.isEmpty() || pass.isEmpty()){
					new AlertDialog.Builder(LoginActivity.this)
					.setTitle(R.string.error)
				    .setMessage(R.string.errorcamposvacios)
				    .setNeutralButton(R.string.cerrar, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				        	
				        }
				     })
				    .setIcon(R.drawable.ic_stat_alert)
				    .show();
		        }
		        else{
			        ObtenerUsuarioAsyncTask tarea = new ObtenerUsuarioAsyncTask();
			        tarea.setActivity(LoginActivity.this);
			        tarea.setContexto(getApplicationContext());
			        tarea.execute(usuario, pass);
		        }		        		        		        		      	       
			}
		});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;                
    }

}
