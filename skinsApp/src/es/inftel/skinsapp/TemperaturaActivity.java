package es.inftel.skinsapp;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class TemperaturaActivity extends Activity implements
		SensorEventListener {
	private SensorManager mSensorManager;
	private Sensor mTemperature;
	float grados;

	private TextToSpeech tts;

	TextView txtView;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		super.setTitle("Temperatura");
		setContentView(R.layout.activity_temperatura);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

		mTemperature = mSensorManager
				.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

		txtView = new TextView(this);
		txtView = (TextView) findViewById(R.id.txtView);

	}

	@Override
	public final void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this,
					BotonesDetailFragment.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public final void onSensorChanged(SensorEvent event) {
		grados = event.values[0];
		Log.i("", "Temperatura -> " + grados);
		String gradosString = Float.toString(grados);
		txtView.setText(gradosString.subSequence(0, 4) + " C");
	}

	@Override
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this, mTemperature,
				SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	public void dictar(View view) {
		tts = new TextToSpeech(getApplicationContext(),
				new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int status) {
						tts.setLanguage(new Locale("es"));
						tts.speak(txtView.getText().toString(),
								TextToSpeech.QUEUE_FLUSH, null);
						String gradosString = Float.toString(grados);
						String textoADictar = (String) gradosString
								.subSequence(0, 4) + " grados";
						tts.speak(textoADictar, TextToSpeech.QUEUE_FLUSH, null);
					}
				});
	}

}