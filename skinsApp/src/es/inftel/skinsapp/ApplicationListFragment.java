package es.inftel.skinsapp;


import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import es.inftel.asynctask.ImageDownloaderTask;
import es.inftel.singleton.UsuarioSingleton;
import es.inftel.utils.TestConexionDispositivo;

public class ApplicationListFragment extends ListFragment {
	
	private static final String STATE_ACTIVATED_POSITION = "activated_position";
	private Callbacks mCallbacks = callBacks;
	private int mActivatedPosition = ListView.INVALID_POSITION;

	public interface Callbacks {
		public void onItemSelected(long l);
	}

	private static Callbacks callBacks = new Callbacks() {
		@Override
		public void onItemSelected(long id) {
		}
	};
	

	class AdaptadorAplicaciones extends ArrayAdapter {
		 
	    Activity context;
	    
	 
	    AdaptadorAplicaciones(Activity context) {
	            super(context, R.layout.application_listitem, UsuarioSingleton.getInstance().getUsuario().getListaApp());
	            
	            this.context = context;
	        }
	 
	        public View getView(int position, View convertView, ViewGroup parent) {
		        LayoutInflater inflater = context.getLayoutInflater();
		        View item = inflater.inflate(R.layout.application_listitem, null);		        		       		       
		        
		        TestConexionDispositivo conexion = new TestConexionDispositivo();
		        if(conexion.hayConexion(getContext())) {
			        ImageView im = (ImageView) item.findViewById(R.id.imageIconoApp);  
			        new ImageDownloaderTask(im).execute(es.inftel.utils.Constants.URL+UsuarioSingleton.getInstance().getUsuario().getListaApp().get(position).getIcono());    
			        
			        TextView lblTitulo = (TextView)item.findViewById(R.id.nameApp);
			        lblTitulo.setText(UsuarioSingleton.getInstance().getUsuario().getListaApp().get(position).getNombre());		        	
		        }
		        else {
			        ImageView im = (ImageView) item.findViewById(R.id.imageIconoApp);  
			        Drawable res = getResources().getDrawable(R.drawable.ic_defecto_apps);
			        im.setImageDrawable(res);
			        new ImageDownloaderTask(im).execute(es.inftel.utils.Constants.URL+UsuarioSingleton.getInstance().getUsuario().getListaApp().get(position).getIcono());    
			        
			        TextView lblTitulo = (TextView)item.findViewById(R.id.nameApp);
			        lblTitulo.setText(UsuarioSingleton.getInstance().getUsuario().getListaApp().get(position).getNombre());			        
		        }		        		        
		        
		        return(item);
		    }
	}	
	
	
	
	public ApplicationListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		

	    super.getActivity().setTitle(R.string.title_app_list);		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {		
			
		View myFragmentView = inflater.inflate(R.layout.fragment_application_list, null);
		
		if(UsuarioSingleton.getInstance().getUsuario().getListaApp().size() == 0){	 						
	        TextView tv = (TextView) myFragmentView.findViewById(R.id.tvNoApps);
	        tv.setText(R.string.noapps);
		}		
		setListAdapter(new AdaptadorAplicaciones(getActivity()));
        return  myFragmentView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (savedInstanceState != null
				&& savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState
					.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
	
		mCallbacks = callBacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position,
			long id) {
		super.onListItemClick(listView, view, position, id);

		mCallbacks.onItemSelected(getListAdapter().getItemId(position));
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {			
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	public void setActivateOnItemClick(boolean activateOnItemClick) {
		getListView().setChoiceMode(
				activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
						: ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
}
