package es.inftel.skinsapp;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import es.inftel.bd.AppSQLite;
import es.inftel.bd.ComponenteSQLite;
import es.inftel.modelo.AccionLlamar;
import es.inftel.modelo.App;
import es.inftel.modelo.Componente;
import es.inftel.modelo.EnviarMensaje;
import es.inftel.singleton.UsuarioSingleton;
import es.inftel.utils.Acciones;

public class GestosDetailFragment extends Fragment{



    private App mItem;
    private TextView descripcionGestos;
    private String descripcion;
	private Componente componente;
    public static final String ARG_ITEM_ID = "item_id";

    public GestosDetailFragment() {
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = UsuarioSingleton.getInstance().getUsuario().getListaApp().get((int) getArguments().getLong(ARG_ITEM_ID));
	        super.getActivity().setTitle(mItem.getNombre());

        }
        
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View v = inflater.inflate(R.layout.activity_realizar_gesto, null, false);
        descripcionGestos = ((TextView) v.findViewById(R.id.tv_descripcion));
        descripcion = "";
    	for(int i=0; i<mItem.getListaComponentes().size(); i++){
    		if(mItem.getListaComponentes().get(i).getTipoEvento() == 2){
    			
    			if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Llamar a ")){
    			    AccionLlamar allam;
    				allam = (AccionLlamar) mItem.getListaComponentes().get(i);
    				descripcion += "-"+mItem.getListaComponentes().get(i).getNombreEvento() + ": " + mItem.getListaComponentes().get(i).getNombreAccion()+" "+allam.getPersona()+"\r\n";
    			}
    			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Dictar temperatura")){
    				descripcion += "-"+mItem.getListaComponentes().get(i).getNombreEvento() + ": " + mItem.getListaComponentes().get(i).getNombreAccion()+"\r\n";
	        	}

    			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Publicar en Facebook")){
    				descripcion += "-"+mItem.getListaComponentes().get(i).getNombreEvento() + ": " + mItem.getListaComponentes().get(i).getNombreAccion()+"\r\n";
    			}
    			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Escribir Tweet")){
    				descripcion += "-"+mItem.getListaComponentes().get(i).getNombreEvento() + ": " + mItem.getListaComponentes().get(i).getNombreAccion()+"\r\n";
    			}
    			else{
    			    EnviarMensaje sms;
    				sms = (EnviarMensaje) mItem.getListaComponentes().get(i);
    				descripcion += "-"+mItem.getListaComponentes().get(i).getNombreEvento() + ": " + mItem.getListaComponentes().get(i).getNombreAccion()+" a "+sms.getPersona()+"\r\n";
    			}
    		}
    	}        

        descripcionGestos.setText(descripcion);

        final GestureDetector gesture = new GestureDetector(getActivity(),
            new GestureDetector.SimpleOnGestureListener() {

	            @Override
	            public boolean onDown(MotionEvent event) {
	                return true;
	            }
	
	            @Override
	            public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
	            	
	            	componente = mItem.getComponentePorGesto("Swipe or Drag");
	            	if(componente != null)
	            	{    		
	            		realizarAccionComponente(componente);
	            	}
	            	else
	            	{
	            		Toast.makeText(getActivity(), R.string.errorgesto, 100).show();
	            	}    	 
	                return true;
	            }
	
	            @Override
	            public void onLongPress(MotionEvent event) {
	            	
	            	componente = mItem.getComponentePorGesto("Long Press");
	            	if(componente != null)
	            	{
	            		realizarAccionComponente(componente);
	            	}
	            	else
	            	{    		
	            		Toast.makeText(getActivity(), R.string.errorgesto, 100).show();
	            	}
	            }
	
	            @Override
	            public boolean onDoubleTap(MotionEvent e) {
	            	
	            	componente = mItem.getComponentePorGesto("Double Touch");
	            	if(componente != null)
	            	{
	            		realizarAccionComponente(componente);
	            	}
	            	else
	            	{
	            		Toast.makeText(getActivity(), R.string.errorgesto, 100).show();    	
	            	}
	            	return false;
	            }
	            
	        	@Override
	        	public boolean onSingleTapConfirmed(MotionEvent arg0) {
	        		
	        		componente = mItem.getComponentePorGesto("Touch");
	            	if(componente != null)
	            	{	
	            		realizarAccionComponente(componente);
	            	}
	            	else
	            	{
	            		Toast.makeText(getActivity(), R.string.errorgesto, 100).show();    	
	            	}
	        		return false;
	        	}
	            
	            @Override
	            public boolean onDoubleTapEvent(MotionEvent e) {
	            	return false;
	            }
	
	
	        	@Override
	        	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
	        			float arg3) {
	        		return false;
	        	}
	
	        	@Override
	        	public void onShowPress(MotionEvent arg0) {     	 
	        	}
	
	        	@Override
	        	public boolean onSingleTapUp(MotionEvent arg0) {
	        		
	        		return false;
	        	}
            });

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gesture.onTouchEvent(event);
            }
        });
        return v;
    }
    
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.detalles, menu);	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()){				
			case R.id.btDescargar: 		      
			      Toast.makeText(getActivity(), R.string.descargaapp, Toast.LENGTH_SHORT).show();
			      
			      AppSQLite appdb = new AppSQLite(this.getActivity(), "dbapp", null, 1, null); 		        
			      appdb.insertarApp(mItem);
			      
			      ComponenteSQLite componentedb = new ComponenteSQLite(this.getActivity(), "dbcomponente", null, 1, null);
			      ArrayList<Componente> comps = mItem.getListaComponentes();
			      for(Componente c : comps) componentedb.insertarComponente(c);		      
			      break;
			}	

		return true;
	}  
	
	private void realizarAccionComponente(Componente c){

		if(c.getNombreAccion().equalsIgnoreCase("Llamar a ")){
			AccionLlamar allam = (AccionLlamar) c;
			Acciones accion = new Acciones();
			accion.realizarLlamada(getActivity(), allam);			
		}
		else if(c.getNombreAccion().equalsIgnoreCase("Dictar temperatura")) {
            Intent temperaturaIntent = new Intent(getActivity(), TemperaturaActivity.class);
            startActivity(temperaturaIntent);
		}
		else if(c.getNombreAccion().equalsIgnoreCase("Publicar en facebook")) {
			Acciones accion = new Acciones();
			accion.abrirFacebook(getActivity());
		}
		else if(c.getNombreAccion().equalsIgnoreCase("Escribir tweet")) {
			Acciones accion = new Acciones();
			accion.abrirTwitter(getActivity());			
		}
		else { 
			EnviarMensaje sms = (EnviarMensaje) c;
			Acciones accion = new Acciones();
			accion.enviarSMS(getActivity(), sms);								
		}
	}	
}
