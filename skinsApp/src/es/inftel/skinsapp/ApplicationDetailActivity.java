package es.inftel.skinsapp;

import java.util.ArrayList;

import es.inftel.bd.AppSQLite;
import es.inftel.bd.ComponenteSQLite;
import es.inftel.modelo.Componente;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class ApplicationDetailActivity extends FragmentActivity implements ActionBar.TabListener {

	
	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application_detail);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setCustomView(R.layout.actionbar);
        
        actionBar.addTab(actionBar.newTab().setText(R.string.botones).setTabListener(this));   
        actionBar.addTab(actionBar.newTab().setText(R.string.gestos).setTabListener(this));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(this, ApplicationListActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {			
	}

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {    	
    	if (tab.getPosition() == 0) {
            Bundle arguments = new Bundle();
            arguments.putLong(BotonesDetailFragment.ARG_ITEM_ID,
                    getIntent().getLongExtra(BotonesDetailFragment.ARG_ITEM_ID, -1));
            BotonesDetailFragment fragment = new BotonesDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.person_detail_container, fragment)
                    .commit();    
            
            
    	} 
    	else if(tab.getPosition() == 1){
            Bundle arguments = new Bundle();
            arguments.putLong(GestosDetailFragment.ARG_ITEM_ID,
                    getIntent().getLongExtra(GestosDetailFragment.ARG_ITEM_ID, -1));

    		
            GestosDetailFragment fragment = new GestosDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.person_detail_container, fragment)
                    .commit();        		
    	}
    }

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
	}  	    
}
