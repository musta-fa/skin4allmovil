package es.inftel.skinsapp;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import es.inftel.bd.AppSQLite;
import es.inftel.bd.ComponenteSQLite;
import es.inftel.modelo.AccionLlamar;
import es.inftel.modelo.App;
import es.inftel.modelo.Componente;
import es.inftel.modelo.EnviarMensaje;
import es.inftel.singleton.UsuarioSingleton;
import es.inftel.utils.Acciones;

public class BotonesDetailFragment extends Fragment {

    public static final String ARG_ITEM_ID = "item_id";

    private App mItem;
    private AccionLlamar allam;
    private EnviarMensaje sms;
    
    private Button[] botones = new Button[8];

    public BotonesDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey(ARG_ITEM_ID)) {
			mItem = UsuarioSingleton.getInstance().getUsuario().getListaApp().get((int) getArguments().getLong(ARG_ITEM_ID));
	        super.getActivity().setTitle(mItem.getNombre());

        }
        
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_application_detail, container, false);

        if (mItem != null) {        	
        	
        	
        	botones[0] = ((Button) rootView.findViewById(R.id.ImageButton01));
        	botones[1] = ((Button) rootView.findViewById(R.id.ImageButton02));
        	botones[2] = ((Button) rootView.findViewById(R.id.ImageButton03));
        	botones[3] = ((Button) rootView.findViewById(R.id.ImageButton04));
        	botones[4] = ((Button) rootView.findViewById(R.id.ImageButton05));
        	botones[5] = ((Button) rootView.findViewById(R.id.ImageButton06));
        	botones[6] = ((Button) rootView.findViewById(R.id.ImageButton07));
        	botones[7] = ((Button) rootView.findViewById(R.id.ImageButton08));
        	
        	int ind = 0;
        	for(int i=0; i<mItem.getListaComponentes().size(); i++){
        		if(mItem.getListaComponentes().get(i).getTipoEvento() == 1){//Comprueba que es un botón
        			botones[ind].setVisibility(1);
        			
        			Drawable image;
        			if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Llamar a ")){
            			if(mItem.getListaComponentes().get(i) instanceof AccionLlamar) {
            				allam = (AccionLlamar) mItem.getListaComponentes().get(i);
                			botones[ind].setText(mItem.getListaComponentes().get(i).getNombreAccion()+allam.getPersona());
                			botones[ind].setOnClickListener(new Button.OnClickListener() {
                			    public void onClick(View v) {
    								Acciones accion = new Acciones();
    								accion.realizarLlamada(getActivity(), allam);                			    	
            			        }
                			});
            			}
        	        	image = getResources().getDrawable(R.drawable.phone);
        			}
        			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Dictar temperatura")){
            			botones[ind].setText(mItem.getListaComponentes().get(i).getNombreAccion());
        	        	image = getResources().getDrawable(R.drawable.thermometer);
        	        	botones[ind].setOnClickListener(new Button.OnClickListener() {
            			    public void onClick(View v) {    			      
        			            Intent temperaturaIntent = new Intent(getActivity(), TemperaturaActivity.class);
        			            startActivity(temperaturaIntent);
        			        }
        			    });
    	        	}

        			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Publicar en Facebook")){
            			botones[ind].setText(mItem.getListaComponentes().get(i).getNombreAccion());
        	        	image = getResources().getDrawable(R.drawable.facebook);
        	        	botones[ind].setOnClickListener(new Button.OnClickListener() {
							@Override
							public void onClick(View arg0) {								
								Acciones accion = new Acciones();
								accion.abrirFacebook(getActivity());
							}        	        		
        	        	});        	        	
        			}
        			else if(mItem.getListaComponentes().get(i).getNombreAccion().equals("Escribir Tweet")){
            			botones[ind].setText(mItem.getListaComponentes().get(i).getNombreAccion());
        	        	image = getResources().getDrawable(R.drawable.twitter);
        	        	botones[ind].setOnClickListener(new Button.OnClickListener() {
							@Override
							public void onClick(View arg0) {
								Acciones accion = new Acciones();
								accion.abrirTwitter(getActivity());
							}        	        		
        	        	});        	        	
        			}
        			else{//Enviar SMS
            			if(mItem.getListaComponentes().get(i) instanceof EnviarMensaje) {
            				sms = (EnviarMensaje) mItem.getListaComponentes().get(i);
                			botones[ind].setText(mItem.getListaComponentes().get(i).getNombreAccion()+" a "+sms.getPersona());
                			botones[ind].setOnClickListener(new Button.OnClickListener() {
                			    public void onClick(View v) {
                					Acciones accion = new Acciones();
                					accion.enviarSMS(getActivity(), sms);
            			        }
                			});            			}
        	        	image = getResources().getDrawable(R.drawable.sms);
        			}
        			botones[ind].setGravity(Gravity.LEFT);
        			image.setBounds( 0, 0, 180, 160 );
    	        	botones[ind].setCompoundDrawables(null, null, image, null);
        			ind++;
        		}
        	}
        }

        return rootView;
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.detalles, menu);	
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){				
			case R.id.btDescargar: 		      
		      Toast.makeText(getActivity(), R.string.descargaapp, Toast.LENGTH_SHORT).show();
		      
		      //mItem es la app
		      AppSQLite appdb = new AppSQLite(this.getActivity(), "dbapp", null, 1, null); 		        
		      appdb.insertarApp(mItem);
		      
		      ComponenteSQLite componentedb = new ComponenteSQLite(this.getActivity(), "dbcomponente", null, 1, null);
		      ArrayList<Componente> comps = mItem.getListaComponentes();
		      for(Componente c : comps) componentedb.insertarComponente(c);		      
		      break;
		}	
		return true;
	} 	
}
