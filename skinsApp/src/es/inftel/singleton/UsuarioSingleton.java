package es.inftel.singleton;

import org.json.JSONException;
import org.json.JSONObject;

import es.inftel.modelo.Usuario;

public class UsuarioSingleton {
	private static UsuarioSingleton INSTANCE = null;
		
		private Usuario usuario;	
		
	    private UsuarioSingleton()
	    {
	    	usuario = new Usuario();
	    }
	 
	
	    private synchronized static void createInstance() {
	        if (INSTANCE == null) { 
	            INSTANCE = new UsuarioSingleton();
	        }
	    }
	
	    public static UsuarioSingleton getInstance() {
	        createInstance();
	        return INSTANCE;
	    }


		public Usuario getUsuario() {
			return usuario;
		}


		public void setUsuario(Usuario usuario) {
			this.usuario = usuario;
			
		}
	    
	    public void init(JSONObject datosUsuario)
	    {
	    	try 
	    	{
				usuario.setId(datosUsuario.getInt("id"));
				usuario.setNombre(datosUsuario.getString("nombre"));
			}
	    	catch (JSONException e) 
			{
				e.printStackTrace();
			}
	    }
	    
	    public void logout(){
	    	usuario.destruirAppsModelo();
	    	usuario = null;
	    }
}